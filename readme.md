Kebutuhan environment:
1. XAMPP dengan PHP >= 5.6
2. [ngrok](https://ngrok.com/)
3. browser

Langkah testing:
1. unduh testing project di https://gitlab.com/codepolitan-lab/opensources/linebot
2. ekstrak project dan simpan di dalam folder htdocs/linebot
3. tes di browser dengan memanggil url http://localhost/linebot. Bila muncul pesan "Siap menerima pesan." berarti kode sudah siap. bila belum muncul, berarti kemungkinan folder belum diekstrak dengan benar
4. buka program cmd dan masuk ke folder tempat aplikasi ngrok disimpan. jalankan ngrok dengan perintah "ngrok.exe http 80"
5. akan muncul url dengan format https://xxxxxxxx.ngrok.io. salin url yang https dan pada browser buka https://xxxxxxxx.ngrok.io/linebot. bila muncul pesan seperti pesan pertama, berarti kode sudah live.
6. Buka aplikasi LINE dan add friend bot testing dengan username @hjm4065e
7. kirim pesan dengan tulisan "get-id" untuk mendapatkan userId LINE
8. panggil url dari browser dengan format https://xxxxxxxx.ngrok.io/linebot/welcome/sendmessage/[userId]/[pesan]. Bila pesan yang ditulis muncul pada bot aplikasi LINE, berarti testing berhasil dan environment siap digunakan.
