<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Curl\Curl;

class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->curl = new Curl();
	}

	public function index()
	{
		$input = json_decode(file_get_contents('php://input'), true);
		if($input){
			print_r($input);
			foreach ($input['events'] as $event) {
				if($event['type'] == 'message'){
					if($event['message']['text'] == 'get-id')
						$this->sendRequest($event, "your id is ".$event['source']['userId']);
					else
						$this->sendRequest($event);
				}
			}
		}
		else
			echo "Siap menerima pesan.";
	}

	function sendmessage($id = false, $message = false)
	{
		if(!$id || !$message)
			exit('Masukkkan userId dan pesan pada url seperti ini:  https://xxxxxxxx.ngrok.io/linebot/welcome/sendmessage/[userId]/[pesan]');

		$event['source']['userId'] = $id;
		$this->sendRequest($event, urldecode($message));
	}

	function sendRequest($event = [], $message = false)
	{
		// set request header
		$this->curl->setHeader('Content-Type', 'application/json');
		$this->curl->setHeader('Authorization', 'Bearer ejyA0Ww41/5kEJbVGU1fqqbm5xlMkOy6CUwYzRUIvUpeZ1I5oBvfxwC7o+jrD1RsUj0JLRF2cEAkNFMFh0CcPUshHjAufI0hgGbyRZJSkFrG3bfxa367MZ8vrgHJZQ6kASUQ79qrpxHZC1BML6lvxgdB04t89/1O/w1cDnyilFU=');

		// set reply data
		$data = [
			'to' => $event['source']['userId'],
			'messages' => [
				["type" => "text", "text" => $message ? $message : $event['message']['text']]
			]
		];

		// send reply data
		$response = $this->curl->post('https://api.line.me/v2/bot/message/push', json_encode($data));

		// for debug purpose
		print_r($response->response);
	}
}
